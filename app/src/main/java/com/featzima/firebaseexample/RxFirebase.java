package com.featzima.firebaseexample;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Cancellable;

public final class RxFirebase {

    private RxFirebase() {
    }

    static Observable<DataSnapshot> valueEventObservable(final DatabaseReference databaseReference) {
        return Observable.create((ObservableEmitter<DataSnapshot> emitter) -> {
            ValueEventListener listener = databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    emitter.onNext(dataSnapshot);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
            emitter.setCancellable(new Cancellable() {
                @Override
                public void cancel() throws Exception {
                    databaseReference.removeEventListener(listener);
                }
            });
        });
    }

    static Observable<FirebaseChild<DataSnapshot>> childEventObservable(final DatabaseReference databaseReference) {
        return Observable.create((ObservableEmitter<FirebaseChild<DataSnapshot>> emitter) -> {
            ChildEventListener listener = databaseReference.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {
                    emitter.onNext(new FirebaseChild(Changes.ADD, dataSnapshot));
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {
                    emitter.onNext(new FirebaseChild(Changes.CHANGE, dataSnapshot));
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                    emitter.onNext(new FirebaseChild(Changes.REMOVE, dataSnapshot));
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String previousChildName) {
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    emitter.onError(databaseError.toException());
                }
            });
            emitter.setCancellable(new Cancellable() {
                @Override
                public void cancel() throws Exception {
                    databaseReference.removeEventListener(listener);
                }
            });
        });
    }

    enum Changes {
        ADD,
        CHANGE,
        REMOVE
    }

    static class FirebaseChild<T> {
        private final Changes changes;
        private final T value;

        public FirebaseChild(Changes changes, T value) {
            this.changes = changes;
            this.value = value;
        }

        public Changes getChanges() {
            return changes;
        }

        public T getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "FirebaseChild{" +
                    "changes=" + changes +
                    ", value=" + value +
                    '}';
        }
    }
}
