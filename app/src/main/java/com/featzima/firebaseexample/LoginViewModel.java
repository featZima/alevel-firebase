package com.featzima.firebaseexample;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

import static com.featzima.firebaseexample.RxLiveData.toObservable;

public class LoginViewModel extends ViewModel {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public final MutableLiveData<String> firstName = new MutableLiveData<>();
    public final MutableLiveData<String> lastName = new MutableLiveData<>();
    public final MutableLiveData<String> fullName = new MutableLiveData<>();
    public final MutableLiveData<Boolean> isValidated = new MutableLiveData<>();
    public final MutableLiveData<String> message = new MutableLiveData<>();
    public final MutableLiveData<List<Note>> notes = new MutableLiveData<>();

    public LoginViewModel() {
        notes.postValue(Collections.emptyList());
        notes.observeForever(notes -> {
            Log.e("!!!", ">>>");
            for (Note note : notes) {
                Log.e("!!!", note.toString());
            }
            Log.e("!!!", "<<<");
        });

        firstName.setValue("");
        lastName.setValue("");
        isValidated.setValue(false);

        Observable<String> fullnameObservable = Observable.combineLatest(
                toObservable(firstName),
                toObservable(lastName),
                (s, s2) -> s + ", " + s2);
        compositeDisposable.add(fullnameObservable.subscribe(fullName::postValue));

        Observable<Boolean> validationObservable = Observable.combineLatest(
                toObservable(firstName),
                toObservable(lastName),
                (s, s2) -> !TextUtils.isEmpty(s) && !TextUtils.isEmpty(s2));
        compositeDisposable.add(validationObservable.subscribe(isValidated::postValue));


        DatabaseReference chatRef = FirebaseDatabase.getInstance()
                .getReference()
                .child("notes");

        compositeDisposable.add(RxFirebase
                .childEventObservable(chatRef)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map(changes -> {
                    DataSnapshot dataSnapshot = changes.getValue();
                    Note note = new Note(dataSnapshot.getKey(), (String) dataSnapshot.child("title").getValue());
                    return new RxFirebase.FirebaseChild<>(changes.getChanges(), note);
                })
                .scan(new ArrayList<Note>(), new BiFunction<List<Note>, RxFirebase.FirebaseChild<Note>, List<Note>>() {
                    @Override
                    public List<Note> apply(List<Note> previousList, RxFirebase.FirebaseChild<Note> child) throws Exception {
                        if (child.getChanges() == RxFirebase.Changes.ADD) {
                            List<Note> newNoteList = new ArrayList<>(previousList);
                            newNoteList.add(child.getValue());
                            return newNoteList;
                        }
                        if (child.getChanges() == RxFirebase.Changes.CHANGE) {
                            List<Note> newNoteList = new ArrayList<>(previousList);
                            newNoteList.set(newNoteList.indexOf(child.getValue()), child.getValue());
                            return newNoteList;
                        }
                        if (child.getChanges() == RxFirebase.Changes.REMOVE) {
                            List<Note> newNoteList = new ArrayList<>(previousList);
                            newNoteList.remove(child.getValue());
                            return newNoteList;
                        }
                        return previousList;
                    }
                })
                .subscribe(noteList -> {
                    notes.postValue(noteList);
                }));

        compositeDisposable.add(Observable
                .create(emitter -> {
                    int counter = 0;
                    while (counter++ < 100) {
                        try {
                            Thread.sleep(1000);
                            emitter.onNext(counter);
                        } catch (InterruptedException e) {
                            // expected behavior
                        }
                    }
                })
                .subscribeOn(Schedulers.computation())
                .subscribe(value -> {
//                    chatRef.setValue(value.toString());
                }));
    }

    public void createNote() {
        String key = UUID.randomUUID().toString();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/notes/" + key + "/title", "New Note");

        FirebaseDatabase.getInstance()
                .getReference()
                .updateChildren(childUpdates);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }

    enum Changes {
        ADD,
        CHANGE,
        REMOVE
    }

    class FirebaseChild {
        private final Changes changes;
        private final DataSnapshot dataSnapshot;

        public FirebaseChild(Changes changes, DataSnapshot dataSnapshot) {
            this.changes = changes;
            this.dataSnapshot = dataSnapshot;
        }

        public Changes getChanges() {
            return changes;
        }

        public DataSnapshot getDataSnapshot() {
            return dataSnapshot;
        }
    }

}
