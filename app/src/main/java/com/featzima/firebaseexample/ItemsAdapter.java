package com.featzima.firebaseexample;

import java.util.List;

public interface ItemsAdapter<T> {

    void setItems(List<T> items);
}
