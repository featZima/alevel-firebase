package com.featzima.firebaseexample;

import androidx.lifecycle.LiveData;
import io.reactivex.Observable;

public final class RxLiveData {

    private RxLiveData() {
    }

    static <T> Observable<T> toObservable(LiveData<T> liveData) {
        return Observable.create(emitter -> {
            liveData.observeForever(value -> {
                emitter.onNext(value);
            });
        });
    }
}
